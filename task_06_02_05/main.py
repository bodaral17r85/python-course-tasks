# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_06_02_05.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


def foo(nums):
    """!!!

    Параметры:
        - nums (list): список.

    Сложность: !!!.
    """
    for x in nums:
        if x % 2 == 0:
            return True
    else:
        return False
